package fun.marsrover.newversion;

import org.junit.Test;

import static fun.marsrover.newversion.Direction.*;
import static fun.marsrover.newversion.Instruction.L;
import static fun.marsrover.newversion.Instruction.R;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by ninan on 8/4/16.
 */
public class RoverTest {


    @Test
    public void aRoverPointingNorthWhenRotatedLeftShouldPointWest() {
        Direction initialDirection = N;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, L) == W);
    }

    @Test
    public void aRoverPointingWestWhenRotatedLeftShouldPointSouth() {
        Direction initialDirection = W;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, L) == S);
    }

    @Test
    public void aRoverPointingSouthWhenRotatedLeftShouldPointEast() {
        Direction initialDirection = S;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, L) == E);
    }

    @Test
    public void aRoverPointingEastWhenRotatedLeftShouldPointNorth() {
        Direction initialDirection = E;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, L) == N);
    }

    @Test
    public void aRoverPointingNorthWhenRotatedRightShouldPointEast() {
        Direction initialDirection = N;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, R) == E);
    }

    @Test
    public void aRoverPointingWestWhenRotatedLeftShouldPointNorth() {
        Direction initialDirection = W;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, R) == N);
    }

    @Test
    public void aRoverPointingSouthWhenRotatedLeftShouldPointWest() {
        Direction initialDirection = S;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, R) == W);
    }

    @Test
    public void aRoverPointingEastWhenRotatedLeftShouldPointSouth() {
        Direction initialDirection = E;
        Rover rover = new Rover(initialDirection);
        assertTrue(rover.directionAfterRotation(initialDirection, R) == S);
    }

    @Test
    public void isTheRoverMovingForward(){
        Rover rover = new Rover(N);
        Cell initialPosition = new Cell(0,0);
        assertEquals(new Cell(0,1), rover.moveForward(initialPosition));
    }


}
