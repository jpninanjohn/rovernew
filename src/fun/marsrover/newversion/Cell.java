package fun.marsrover.newversion;

import static fun.marsrover.newversion.Direction.*;

/**
 * Created by ninan on 8/5/16.
 */
public class Cell {
    private final int xCoordinate;
    private final int yCoordinate;

    public Cell(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public Cell getNeighbourBasedOnDirection(Direction direction) {
        if (direction == N) {
            return new Cell(xCoordinate, yCoordinate + 1);
        }
        if (direction == S) {
            return new Cell(xCoordinate, yCoordinate - 1);
        }
        if (direction == E) {
            return new Cell(xCoordinate + 1, yCoordinate);
        }
        return new Cell(xCoordinate - 1, yCoordinate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        if (xCoordinate != cell.xCoordinate) return false;
        return yCoordinate == cell.yCoordinate;

    }

    @Override
    public String toString() {
        return "Cell{" +
                "xCoordinate=" + xCoordinate +
                ", yCoordinate=" + yCoordinate +
                '}';
    }

    @Override
    public int hashCode() {
        int result = xCoordinate;
        result = 31 * result + yCoordinate;
        return result;
    }
}
