package fun.marsrover.newversion;

import java.util.*;

import static fun.marsrover.newversion.Direction.*;
import static fun.marsrover.newversion.Instruction.*;

/**
 *
 */

public class Rover {

    private Cell currentCell;
    private List<Instruction> instructionList;
    private Direction direction;

    private final Map<Direction, Direction> leftRotation;
    private final Map<Direction, Direction> rightRotation;

    public Rover(Direction direction) {
        this(direction, new Cell(0, 0), "");
    }

    public Rover(Direction direction, Cell cell, String instSet) {
        this.direction = direction;
        this.currentCell = cell;
        instructionList = new ArrayList<>();
        parseInstructions(instSet);
        leftRotation = new HashMap<>();
        rightRotation = new HashMap<>();
        setDirectionForLeftRotation();
        setDirectionForRightRotation();
    }

    private void setDirectionForRightRotation() {
        rightRotation.put(N, E);
        rightRotation.put(E, S);
        rightRotation.put(S, W);
        rightRotation.put(W, N);
    }

    private void setDirectionForLeftRotation() {
        leftRotation.put(N, W);
        leftRotation.put(W, S);
        leftRotation.put(S, E);
        leftRotation.put(E, N);
    }

    private void executeInstructions() {
        for (Instruction instruction : instructionList) {
            if (instruction == L || instruction == R) {
                direction = directionAfterRotation(direction, instruction);
            }
            if (instruction == M) {
                currentCell = moveForward(currentCell);
            }
        }
    }

    private List<Instruction> parseInstructions(String instSet) {
        List<Instruction> instructionList = new ArrayList<>();
        int i = 0;
        while (i < instSet.length()) {
            String inst = String.valueOf(instSet.charAt(i));
            instructionList.add(Instruction.valueOf(inst));
            i++;
        }
        this.instructionList.addAll(instructionList);
        return instructionList;
    }

    public Direction directionAfterRotation(Direction currentDirection, Instruction instruction) {
        if (instruction == L) {
            return leftRotation.get(currentDirection);
        }
        if (instruction == R) {
            return rightRotation.get(currentDirection);
        }
        return currentDirection;
    }

    public Cell moveForward(Cell currentPosition) {
        Cell nextCell = currentPosition.getNeighbourBasedOnDirection(direction);
        return nextCell;
    }

    @Override
    public String toString() {
        return "Rover{" +
                "Direction=" + direction +
                ", currentCell=" + currentCell +
                '}';
    }

    public static void main(String... a) {

        Scanner in = new Scanner(System.in);

        int xCoordinate, yCoordinate;
        String instSet;

        String direction;
        System.out.println("Enter the direction the rover is Headed");
        direction = in.nextLine();

        System.out.println("Enter the Initial XLocation:");
        xCoordinate = in.nextInt();
        in.nextLine();

        System.out.println("Enter the Initial YLocation:");
        yCoordinate = in.nextInt();
        in.nextLine();

        System.out.println("Enter the Instructions\t");
        instSet = in.nextLine();

        Rover newRover = new Rover(Direction.valueOf(direction), new Cell(xCoordinate, yCoordinate), instSet);
        newRover.executeInstructions();
        System.out.println(newRover);
    }

}
